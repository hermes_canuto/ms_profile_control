package config

import (
	"os"
	"time"
)

var timeout time.Duration

func SetTimeOut(){
	timeout, _ = time.ParseDuration(os.Getenv("TIMEOUT"))
}

func GetTimeOut() time.Duration {
	return timeout
}

const (
	TABLENAME   = "ProfileControl"
	WRITE       = "write"
	READ        = "read"
	DENY        = "deny"
)

type Config struct {
	Secret  string
	Port    string
}

type ItemFunction struct {
	ID         string `json:"id"`
	Permission string `json:"permission"`
}

type Item struct {
	Application string         `json:"application,omitempty"`
	Profile     string         `json:"profile,omitempty"`
	All         string         `json:"all,omitempty"`
	Function    []ItemFunction `json:"function"`
}


