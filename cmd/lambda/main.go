package main

import (
	"bolerplate-restfull/api"
	"bolerplate-restfull/config"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	echoadapter "github.com/awslabs/aws-lambda-go-api-proxy/echo"
	"os"
)

var echolambda *echoadapter.EchoLambda


func init(){


	config := config.Config{
		Secret:        os.Getenv("JWT_SECRET"),
		Port: ":8000",
	}


	e := api.New(config)
	e.Logger.Print("JWT_SECRET", config.Secret)
	e.Logger.Print("Http server API", config.Port)
	echolambda = echoadapter.New(e)
}

func Handler(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	// If no name is provided in the HTTP request body, throw an error
	return echolambda.ProxyWithContext(ctx, req)
}

func main(){
	lambda.Start(Handler)
}