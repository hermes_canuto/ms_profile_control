package main

import (
	"bolerplate-restfull/amazon"
	"bolerplate-restfull/app"
	"bolerplate-restfull/config"
	"os"
)

func main() {

	// setting dynamoDB
	// amazon session
	amazon.NewDynamoDB()
	config.SetTimeOut()

	config := config.Config{
		Secret:        os.Getenv("JWT_SECRET"),
		Port: ":8000",
	}

	app.Start(config)
}
