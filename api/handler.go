package api

import (
	"bolerplate-restfull/amazon"
	"bolerplate-restfull/config"
	"github.com/labstack/echo/v4"
	"net/http"
)

type functions []map[string]string

type Post struct {
	Application string `json:"application"`
	Role        string `json:"role"`
}

type Response struct {
	Role     string              `json:"role"`
	All      string              `json:"all"`
	Function []map[string]string `json:"function"`
}

func show(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{"Hello": "I´m alive"})
}

func healthcheck(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{"status": "available"})
}


func getprofile(c echo.Context) error {

	p := new(Post)
	if err := c.Bind(p); err != nil {
		return err
	}
	c.Logger().Print("getprofile ", p.Role, " ", p.Application)

	item, err := amazon.GetItem(p.Role, p.Application, c)
	if err != nil {
		return c.JSON(http.StatusNotFound, map[string]string{"msg": err.Error()})
	}
	return c.JSON(http.StatusOK, item)
}

func createProfile(c echo.Context) error {

	p := new(config.Item)
	if err := c.Bind(p); err != nil {
		return err
	}
	c.Logger().Print("createProfile ", *p)
	item, err := amazon.CreateItem(*p, c)
	if err != nil {
		return c.JSON(http.StatusNotFound, map[string]string{"msg": err.Error()})
	}
	return c.JSON(http.StatusOK, item)
}eu