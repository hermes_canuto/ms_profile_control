package api

import (
	"bolerplate-restfull/config"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func setRoutes(config config.Config,api *echo.Echo) {

	api.Logger.Print("Setting up Routes")
	api.GET("/", show)
	api.GET("/v1/healthcheck", healthcheck)

	handler := func(c echo.Context) error {
		return getprofile(c)
	}

	// Restricted area
	secret := config.Secret
	r := api.Group("/api/v1/")
	r.Use(middleware.JWT([]byte(secret)))

	r.POST("getprofile", handler)
	r.POST("createorupdateprofile", createProfile)
}
