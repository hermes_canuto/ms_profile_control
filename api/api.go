package api

import (
	"bolerplate-restfull/config"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"time"
)

func New(config config.Config) *echo.Echo {
	e := echo.New()
	e.Use(middleware.Recover())
	e.Server.Addr = config.Port
	e.Server.ReadTimeout = 30 * time.Second
	setRoutes(config,e) // setting up the routes
	return e
}
