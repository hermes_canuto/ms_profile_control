FROM golang:latest as builder
RUN go version
WORKDIR /app
COPY . .

# run test
RUN go get -d -v ./...
# Install the package
RUN CGO_ENABLED=0 go install -ldflags '-s' -a -installsuffix cgo  -v ./...

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /go/bin/myapp .

EXPOSE 8000
ENTRYPOINT ["/app/myapp"]

