
lambda:
	echo "Building lambda"
	GOARCH=amd64 GOOS=linux go build -ldflags="-s -w" -o build/lambda  cmd/lambda/main.go
	sam build
	sam local start-api

local:
	echo "Building local"
	GOARCH=amd64 GOOS=linux go build -ldflags="-s -w" -o build/local  cmd/myapp/main.go