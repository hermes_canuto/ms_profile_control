# local

provider "aws" {
  region = "us-east-1"
  access_key = "foo"
  secret_key = "bar"
  skip_credentials_validation = true
  skip_requesting_account_id = true
  skip_metadata_api_check = true
  s3_force_path_style = true
  endpoints {
    s3 = "http://localhost:4566"
    lambda = "http://localhost:4566"
    sqs = "http://localhost:4566"
    dynamodb = "http://localhost:4566"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "bucket01"
  acl    = "public-read"
}

resource "aws_sqs_queue" "q" {
  name = "queue01"
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name              = "ProfileControl"
  billing_mode      = "PROVISIONED"
  read_capacity     = 20
  write_capacity    = 20
  hash_key          = "application"
  range_key         = "profile"

  attribute {
    name = "application"
    type = "S"
  }

  attribute {
    name = "profile"
    type = "S"
  }
	
}