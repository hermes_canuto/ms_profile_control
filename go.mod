module bolerplate-restfull

go 1.16

require (
	github.com/aws/aws-lambda-go v1.24.0 // indirect
	github.com/aws/aws-sdk-go v1.39.5 // indirect
	github.com/aws/aws-sdk-go-v2 v1.7.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.4.1 // indirect
	github.com/awslabs/aws-lambda-go-api-proxy v0.10.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/labstack/echo/v4 v4.3.0
	github.com/tylerb/graceful v1.2.15
	golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6 // indirect
)
