package amazon

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"log"
	"os"
)

func getAmazonSession(l bool) (*session.Session, error) {
	var sess *session.Session
	var err error
	//var timeout = time.Duration(2 * time.Second)
	localstack := os.Getenv("LOCAL")
	if localstack != "" {
		log.Println("Running Localstack", localstack)
		//localstack
		sess, err = session.NewSession(&aws.Config{
			Region:   aws.String(endpoints.UsEast1RegionID),
			Endpoint: aws.String(localstack),
		})

		//if l { http://192.168.0.6:4566

		//	sess.Handlers.Send.PushFront(func(r *request.Request) {
		//		log.Println(r.ClientInfo.ServiceName, r.Operation, r.Params)
		//	})
		//}
		if err != nil {
			if awsErr, ok := err.(awserr.Error); ok {
				return sess, awsErr
			}

		}
	} else {
		sess = session.Must(session.NewSessionWithOptions(session.Options{
			SharedConfigState: session.SharedConfigEnable,
		}))

	}
	return sess, nil
}
