package amazon

import (
	"bolerplate-restfull/config"
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/labstack/echo/v4"
	"log"
)

var dynamoDB *dynamodb.DynamoDB

// NewDynamoDB Create a Session with DynamoDB
func NewDynamoDB() {
	sess, err := getAmazonSession(false)
	if err != nil {
		log.Fatal(err.Error())
	}
	dynamoDB = dynamodb.New(sess)
}

// GetDynamoDB get the currente dynamoDB session
func GetDynamoDB() *dynamodb.DynamoDB {
	return dynamoDB
}

// GetItem get a profile
// role : user role
// application : user application
// return error
func GetItem(role, application string, c echo.Context) (config.Item, error) {

	var item config.Item
	db := GetDynamoDB()

	filt := expression.Name("profile").Equal(expression.Value(role)).
		And(expression.Name("application").Equal(expression.Value(application)))

	proj := expression.NamesList(
		expression.Name("all"),
		expression.Name("function"),
	)
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		return item, errors.New(fmt.Sprintf("Got error building expression: %s", err))
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(config.TABLENAME),
	}

	ctx, cancelFn := context.WithTimeout(context.Background(),  config.GetTimeOut())
	defer cancelFn()

	result, err := db.ScanWithContext(ctx, params)
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			return item, errors.New("  -> " + awsErr.Message())
		}
		return item, err
	}

	for _, i := range result.Items {
		err = dynamodbattribute.UnmarshalMap(i, &item)
		if err != nil {
			return item, errors.New(err.Error())
		}

		return item, nil
	}

	return item, errors.New("not found")
}

func CreateItem(item config.Item, c echo.Context) (config.Item, error) {
	db := GetDynamoDB()
	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		log.Fatalf("Got error marshalling new movie item: %s", err)
	}
	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(config.TABLENAME),
	}

	ctx, cancelFn := context.WithTimeout(context.Background(), config.GetTimeOut())
	defer cancelFn()

	_, err = db.PutItemWithContext(ctx, input)
	if err != nil {
		fmt.Println(err.Error())
		fmt.Printf("Got error calling PutItem: %s", err)
		return item, err
	}
	return item, nil
}
