package app

import (
	"bolerplate-restfull/api"
	"bolerplate-restfull/config"
	"fmt"
	"github.com/tylerb/graceful"
	"time"
)

func Start(config config.Config){
	e := api.New(config)
	e.Logger.Print("Http server API", config.Port)
	graceful.ListenAndServe(e.Server, 5*time.Second)
	fmt.Println("Go away")
}




